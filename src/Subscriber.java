import java.util.Observable;
import java.util.Observer;

public class Subscriber implements Observer {
	String name;
	
	public Subscriber(String name) {
		this.name = name;
	}
		
		public void sayHello() {
			System.out.println("This is subscriber");
		}
		
		
	

	public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	@Override
	public void update(Observable o, Object arg) {
		
		System.out.println("Todays updated wheater is ");
		System.out.println("Temperature"+arg.toString());
		
	}
	

}
