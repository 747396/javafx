
import java.util.Observable;
// Change this to a publisher
public class Publisher extends Observable {
	private int temp;
	public Publisher(int temp) {
		this.temp = temp;
	}
	public void sayhello() {
		System.out.println("I am a publisher"+ this.temp);
	}
	public int getTemp() {
		return temp;
	}
	public void setTemp(int temp) {
		this.temp = temp;
	
	
	
	this.setChanged();
	this.notifyObservers();
	
	

}
}
