import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class JavaFxToday extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//1.Add User Interface Controls
		Label quantity = new Label("quantity for shirts requested");
		
	    TextField quantityTextBox = new TextField();
	    
	    Label subtotalbeforeLabel = new Label("price before tax");
	     
	   
	    
	   Label totaltax = new Label("Total Tax");
	  
	   
	    Label FinalTotal= new Label("FinalTotal");
	  
	    
	    
	    Button btn = new Button();
	    btn.setText("Click Me!");
	    
	   //add event handler
	    
	    btn.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent event) {
				
				System.out.println("Click Me!");
				
				String Q1 = quantityTextBox.getText();
				System.out.println(" " + quantity);
				
				
				
				
				int Quantity = Integer.parseInt(Q1);
				
				
				
				double price = 0;
				
				if(Quantity>=1 && Quantity<=5) {
					price = Quantity*5;
					
					
				}else if(Quantity>=6 && Quantity<=100){
					price = Quantity*2;
					
					
				}else if(Quantity > 100){
					price = Quantity*0.50;
					
					
				}
				quantity.setText("The number of t-shirts" + Quantity);
				subtotalbeforeLabel.setText("The subtotal is " + price);
				
				 
				double ToTotal = price*0.13;
				totaltax.setText("The tax amount is "+ ToTotal);
				
				double FinatTotal = price + ToTotal;
				FinalTotal.setText("the final price is "+ FinatTotal);
				
				
				
				
				
				
				
				//Clear after
				quantityTextBox.setText("");
				
				
				
				
				
				
				
				
				
			
				
			}
	    	
	    	
	    });
	    
	    //2. ADD LAYOUT MANAGER
	   
	    VBox root = new VBox();
	    root.setSpacing(10);
	    
	    //3. ADD UI CONTROLS TO THE LAYOUT MANAGER 
	    
	    root.getChildren().add(quantity);
	    root.getChildren().add(quantityTextBox);
	   
	   
	    root.getChildren().add(subtotalbeforeLabel);
	    
	    
	    root.getChildren().add(totaltax);
	   
	    
	   
	    
	    root.getChildren().add(btn);
	    root.getChildren().add(FinalTotal);
	   
	    
	    
	    
	   
	    
	   
	    
	   
	    
	    //ADD SCENE TO STAGE AND LAYOUT MANGER TO SCENE
	    
	    primaryStage.setScene(new Scene(root,300,300));
	    
	    
	    //SET TITLE BAR OF THE APP
	    
	    primaryStage.setTitle("Tshirts price calculator");
	    
	    //Show Stage
	    
	    primaryStage.show();
	    
	    
	    
	    
	    
	    
	    
	    
	    
		
		
	}

	
}

